package com.example.vehicle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://ngopenshift-test-app.apps.vrls2.5c0f.sandbox1183.opentlc.com")
public class VehicleController {


    @GetMapping("/vehicles")
    public List<String> getVehicles() {
        List<String> vehicles = new ArrayList<>(Arrays.asList("Vehicle 1","Vehicle 2","Vehicle 3"));
        return vehicles;
    }




}